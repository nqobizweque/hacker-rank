package com.hackerank.app;

import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    static void rotateMatrix(List<List<Integer>> matrix, int r) {
        int ringCount = getRingCount(matrix);
        for (int i = 0; i < ringCount; i++) {
            rotateRing(matrix, i, r);
        }
        printMatrix(matrix);
    }

    static  int getRingCount(List<List<Integer>> matrix) {
        int ringCount = Integer.min(matrix.size(), matrix.get(0).size());
        return ringCount == 1 ? 1 : ringCount / 2;
    }

    static void rotateRing(List<List<Integer>> matrix, int rowColumnIndex, int rotationCount) {
        List<int[]> coordinates = getCoordinates(matrix, rowColumnIndex);
        int[] heightWidth = getRingHeightWidth(matrix, rowColumnIndex);
        int rowCount = heightWidth[0], columnCount = heightWidth[1];
        int totalRingItems = getRingElementCount(heightWidth[0], heightWidth[1]);
        for (rotationCount = getOptimizedTotalRotations(totalRingItems, rotationCount); rotationCount > 0; rotationCount--) {
            int element = Integer.MIN_VALUE;
            for (int fromRow = coordinates.get(0)[0], toRow = coordinates.get(1)[0] + (columnCount <= 1 && rowCount > 1 ? 1 : 0), column = rowColumnIndex; (rowCount > 1 && fromRow < toRow) || fromRow == 0; fromRow++) {
                element = replace(matrix, fromRow, column, element);
            }
            for (int row = coordinates.get(1)[0], fromColumn = coordinates.get(1)[1], toColumn = coordinates.get(2)[1]; rowCount > 1 && columnCount > 1 && fromColumn < toColumn; fromColumn++) {
                element = replace(matrix, row, fromColumn, element);
            }
            for (int fromRow = coordinates.get(2)[0], toRow = coordinates.get(3)[0], column = coordinates.get(2)[1]; rowCount > 1 && columnCount > 1 && fromRow > toRow; fromRow--) {
                element = replace(matrix, fromRow, column, element);
            }
            for (int row = coordinates.get(3)[0], fromColumn = coordinates.get(3)[1], toColumn = coordinates.get(0)[0]; fromColumn > toColumn; fromColumn--) {
                element = replace(matrix, row, fromColumn, element);
            }
            replace(matrix, rowColumnIndex, rowColumnIndex, element);
        }
    }

    static List<int[]> getCoordinates(List<List<Integer>> matrix, int rowColumnIndex) {
        int[] heightWidth = getRingHeightWidth(matrix, rowColumnIndex);
        int rowCount = heightWidth[0], columnCount = heightWidth[1];
        // TOP LEFT
        List<int[]> coordinates = new ArrayList<>();
        coordinates.add(new int[]{rowColumnIndex, rowColumnIndex});
        // BOTTOM LEFT
        if (rowCount <= 1) {
            coordinates.add(coordinates.get(0));
        } else {
            coordinates.add(new int[] {rowColumnIndex + (rowCount - 1), rowColumnIndex});
        }
        // BOTTOM RIGHT
        if (columnCount <= 1) {
            coordinates.add(coordinates.get(1));
        } else {
            coordinates.add(new int[] { rowColumnIndex + (rowCount - 1), rowColumnIndex + (columnCount - 1)});
        }
        // TOP RIGHT
        if (columnCount <= 1) {
            coordinates.add(coordinates.get(0));
        } else {
            coordinates.add(new int[] {rowColumnIndex, rowColumnIndex + (columnCount - 1)});
        }
        return coordinates;
    }

    static int[] getRingHeightWidth(List<List<Integer>> matrix, int rowColumnIndex) {
        return new int[] {matrix.size() - (rowColumnIndex * 2), matrix.get(0).size() - (rowColumnIndex * 2)};
    }

    static int getRingElementCount(int height, int width) {
        if (height == 1) {
            return width;
        }
        if (width == 1) {
            return height;
        }
        return (height - 1) * 2 + (width - 1) * 2;
    }

    static int getOptimizedTotalRotations(int elementCount, int rotations) {
        return rotations % elementCount;
    }

    static void printMatrix(List<List<Integer>> matrix) {
        for(List<Integer> row: matrix) {
            for (int i = 0; i < row.size(); i++) {
                System.out.print(row.get(i) + " ");
            }
            System.out.println();
        }
        for (int i = 0; i < matrix.get(0).size(); i++) {
            System.out.print("- ");
        }
        System.out.println();
    }

    static Integer replace(List<List<Integer>> matrix, int rowIndex, int columnIndex, int element) {
        return matrix.get(rowIndex).set(columnIndex, element);
    }
}
