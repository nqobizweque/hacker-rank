package com.hackerank.app;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class NextPositionTest {
    @Parameterized.Parameter(0)
    public int rowCount;

    @Parameterized.Parameter(1)
    public int columnCount;

    @Parameterized.Parameter(2)
    public int rowIndex;

    @Parameterized.Parameter(3)
    public int columnIndex;

    @Parameterized.Parameter(4)
    public int nextRowIndex;

    @Parameterized.Parameter(5)
    public int nextColumnIndex;

    @Parameterized.Parameters
    public static Collection<Integer[]> data() {
        return Arrays.asList(new Integer[][]{
                {4, 4, 0, 0, 1, 0},
                {4, 4, 0, 1, 0, 0},
                {4, 4, 0, 2, 0, 1},
                {4, 4, 0, 3, 0, 2},

                {4, 4, 1, 0, 2, 0},
                {4, 4, 1, 1, 2, 1},
                {4, 4, 1, 2, 1, 1},
                {4, 4, 1, 3, 0, 3},

                {4, 4, 2, 2, 1, 2},
        });
    }

}
