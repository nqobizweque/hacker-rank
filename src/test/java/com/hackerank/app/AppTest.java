package com.hackerank.app;

import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class AppTest 
{
    private Hashtable<String, List<List<Integer>>> matrixDict;

    @Before
    public void setup() throws FileNotFoundException {
        Scanner input = new Scanner(new FileReader("test_input.dat"));
        matrixDict = new Hashtable<>();
        while(input.hasNext()) {
            String line = input.nextLine().replaceFirst("#", "");
            String[] matrixDefinition = line.split(" ");
            String matrixKey = matrixDefinition[0];
            int rowCount = Integer.parseInt(matrixDefinition[1]);
            List<List<Integer>> matrix = new ArrayList<>();
            for (int i = 0; i < rowCount; i++) {
                line = input.nextLine();
                matrix.add(Arrays.stream(Arrays.asList(line.split(" ")).stream().mapToInt(Integer::parseInt).toArray()).boxed().collect(Collectors.toList()));
            }
            matrixDict.put(matrixKey, matrix);
        }
    }

    @Test
    public void test_getRingHeightWidth() {
        List<List<Integer>> matrix = matrixDict.get("1");
        int[] heightWidth = App.getRingHeightWidth(matrix, 1);
        assertEquals(3, heightWidth[0]);
        assertEquals(2, heightWidth[1]);
        heightWidth = App.getRingHeightWidth(matrix, 0);
        assertEquals(5, heightWidth[0]);
        assertEquals(4, heightWidth[1]);

        matrix = matrixDict.get("3");
        heightWidth = App.getRingHeightWidth(matrix, 0);
        assertEquals(3, heightWidth[0]);
        assertEquals(3, heightWidth[1]);
        heightWidth = App.getRingHeightWidth(matrix, 1);
        assertEquals(1, heightWidth[0]);
        assertEquals(1, heightWidth[1]);

        matrix = matrixDict.get("4");
        heightWidth = App.getRingHeightWidth(matrix, 0);
        assertEquals(3, heightWidth[0]);
        assertEquals(4, heightWidth[1]);
        heightWidth = App.getRingHeightWidth(matrix, 1);
        assertEquals(1, heightWidth[0]);
        assertEquals(2, heightWidth[1]);

        matrix = matrixDict.get("5");
        heightWidth = App.getRingHeightWidth(matrix, 0);
        assertEquals(4, heightWidth[0]);
        assertEquals(3, heightWidth[1]);
        heightWidth = App.getRingHeightWidth(matrix, 1);
        assertEquals(2, heightWidth[0]);
        assertEquals(1, heightWidth[1]);

        matrix = matrixDict.get("6");
        heightWidth = App.getRingHeightWidth(matrix, 0);
        assertEquals(1, heightWidth[0]);
        assertEquals(3, heightWidth[1]);

        matrix = matrixDict.get("7");
        heightWidth = App.getRingHeightWidth(matrix, 0);
        assertEquals(3, heightWidth[0]);
        assertEquals(1, heightWidth[1]);

        matrix = matrixDict.get("8");
        heightWidth = App.getRingHeightWidth(matrix, 0);
        assertEquals(1, heightWidth[0]);
        assertEquals(1, heightWidth[1]);
    }

    @Test
    public void test_getCoordinates() {
        List<List<Integer>> matrix = matrixDict.get("1");
        List<int[]> coordinates = App.getCoordinates(matrix, 0);
        List<int[]> expectedCoordiates = Arrays.asList(
                new int[] {0, 0},
                new int[] {4, 0},
                new int[] {4, 3},
                new int[] {0, 3}
        );
        assertCoordinates(coordinates, expectedCoordiates);
        coordinates = App.getCoordinates(matrix, 1);
        expectedCoordiates = Arrays.asList(
                new int[] {1, 1},
                new int[] {3, 1},
                new int[] {3, 2},
                new int[] {1, 2}
        );
        assertCoordinates(coordinates, expectedCoordiates);

        matrix = matrixDict.get("3");
        coordinates = App.getCoordinates(matrix, 1);
        expectedCoordiates = Arrays.asList(
                new int[] {1, 1},
                new int[] {1, 1},
                new int[] {1, 1},
                new int[] {1, 1}
        );
        assertCoordinates(coordinates, expectedCoordiates);

        matrix = matrixDict.get("6");
        coordinates = App.getCoordinates(matrix, 0);
        expectedCoordiates = Arrays.asList(
                new int[] {0, 0},
                new int[] {0, 0},
                new int[] {0, 2},
                new int[] {0, 2}
        );
        assertCoordinates(coordinates, expectedCoordiates);

        matrix = matrixDict.get("7");
        coordinates = App.getCoordinates(matrix, 0);
        expectedCoordiates = Arrays.asList(
                new int[] {0, 0},
                new int[] {2, 0},
                new int[] {2, 0},
                new int[] {0, 0}
        );
        assertCoordinates(coordinates, expectedCoordiates);

        matrix = matrixDict.get("8");
        coordinates = App.getCoordinates(matrix, 0);
        expectedCoordiates = Arrays.asList(
                new int[] {0, 0},
                new int[] {0, 0},
                new int[] {0, 0},
                new int[] {0, 0}
        );
        assertCoordinates(coordinates, expectedCoordiates);
    }

    private void assertCoordinates(List<int[]> coordinates, List<int[]> expectedCoordiates) {
        for (int i = 0; i < coordinates.size(); i++) {
            int[] expectedCoordinate = expectedCoordiates.get(i),
                    returnedCoordinate = coordinates.get(i);
            assertEquals(expectedCoordinate[0], returnedCoordinate[0]);
            assertEquals(expectedCoordinate[1], returnedCoordinate[1]);
        }
    }

    @Test
    public void test_ringItemCount() {
        List<List<Integer>> matrix = matrixDict.get("1");
        int[] heightWidth = App.getRingHeightWidth(matrix, 1);
        int itemCount = App.getRingElementCount(heightWidth[0], heightWidth[1]);
        assertEquals(6, itemCount);
        heightWidth = App.getRingHeightWidth(matrix, 0);
        itemCount = App.getRingElementCount(heightWidth[0], heightWidth[1]);
        assertEquals(14, itemCount);
        matrix = matrixDict.get("6");
        heightWidth = App.getRingHeightWidth(matrix, 0);
        itemCount = App.getRingElementCount(heightWidth[0], heightWidth[1]);
        assertEquals(3, itemCount);
    }

    @Test
    public void getOptimizedTotalRotations() {
        int rotations = App.getOptimizedTotalRotations(5, 7);
        assertEquals(2, rotations);
        rotations = App.getOptimizedTotalRotations(5, 10);
        assertEquals(0, rotations);
        rotations = App.getOptimizedTotalRotations(5, 2);
        assertEquals(2, rotations);
    }

    @Test
    public void test_rotateRing() {
        List<List<Integer>> matrix = matrixDict.get("1"),
                outputMatrix = matrixDict.get("11");
        App.rotateRing(matrix, 0, 2);
        test_rotatedRingSimilarity(matrix, outputMatrix, 0);

        matrix = matrixDict.get("6");
        App.rotateRing(matrix, 0 , 1);
        assertEquals((Integer) 64,matrix.get(0).get(0));
        assertEquals((Integer) 88,matrix.get(0).get(1));
        assertEquals((Integer) 12,matrix.get(0).get(2));

        matrix = matrixDict.get("7");
        App.rotateRing(matrix, 0 , 1);
        assertEquals((Integer) 88,matrix.get(0).get(0));
        assertEquals((Integer) 12,matrix.get(1).get(0));
        assertEquals((Integer) 64,matrix.get(2).get(0));

    }
    
    private void test_rotatedRingSimilarity(List<List<Integer>> matrix, List<List<Integer>> outputMatrix, int rowColumnIndex) {
        List<int[]> coordinates = App.getCoordinates(matrix, rowColumnIndex);
        int[] topLeft = coordinates.get(0),
                bottomLeft = coordinates.get(1),
                bottomRight = coordinates.get(2),
                topRight = coordinates.get(3);
        for (int rowFrom = topLeft[0], rowTo = bottomLeft[0], column = topLeft[1]; rowFrom < rowTo; rowFrom++) {
            assertEquals(matrix.get(rowFrom).get(column), outputMatrix.get(rowFrom).get(column));
        }
    }
    
    private void test_matrixSimilarityByRing(List<List<Integer>> matrix, List<List<Integer>> outputMatrix, int rowColumnIndex) {
        List<int[]> coordinates = App.getCoordinates(matrix, rowColumnIndex);
        int[] topLeft = coordinates.get(0),
                bottomLeft = coordinates.get(1),
                bottomRight = coordinates.get(2),
                topRight = coordinates.get(3);
        for (int rowFrom = topLeft[0], rowTo = bottomLeft[0], column = topLeft[1]; rowFrom < rowTo; rowFrom++) {
            assertEquals(matrix.get(rowFrom).get(column), outputMatrix.get(rowFrom).get(column));
        }
        for (int row = bottomLeft[0], fromColumn = bottomLeft[1], toColumn = bottomRight[1]; fromColumn < toColumn; fromColumn++) {
            assertEquals(matrix.get(row).get(fromColumn), outputMatrix.get(row).get(fromColumn));
        }
        for (int rowFrom = bottomRight[0], rowTo = topRight[0], column = bottomRight[1]; rowFrom > rowTo; rowFrom--) {
            assertEquals(matrix.get(rowFrom).get(column), outputMatrix.get(rowFrom).get(column));
        }
        for (int row = topRight[0], fromColumn = topRight[1], toColumn = topLeft[1]; fromColumn > toColumn; fromColumn--) {
            assertEquals(matrix.get(row).get(fromColumn), outputMatrix.get(row).get(fromColumn));
        }
    }

    private void assert_matrixSimilarity(List<List<Integer>> inputMatrix, List<List<Integer>> outputMatrix) {
        for (int row = 0; row < inputMatrix.size(); row++) {
            for (int column = 0; column < inputMatrix.get(row).size(); column++) {
                assertEquals(inputMatrix.get(row).get(column), outputMatrix.get(row).get(column));
            }
        }
    }

    @Test
    public void test_rotateMatrix() {
        List<List<Integer>> inputMatrix = matrixDict.get("1"),
                outputMatrix = matrixDict.get("11");
        App.rotateMatrix(inputMatrix, 2);
        assert_matrixSimilarity(inputMatrix, outputMatrix);

        inputMatrix = matrixDict.get("12");
        outputMatrix = matrixDict.get("13");
        App.rotateMatrix(inputMatrix, 1);
        assert_matrixSimilarity(inputMatrix, outputMatrix);

        inputMatrix = matrixDict.get("15");
        outputMatrix = matrixDict.get("16");
        App.rotateMatrix(inputMatrix, 7);
        assert_matrixSimilarity(inputMatrix, outputMatrix);
    }

    @Test
    public void test_getRingCount() {
        List<List<Integer>> matrix = matrixDict.get("1");
        assertEquals(2, App.getRingCount(matrix));
        matrix = matrixDict.get("14");
        assertEquals(1, App.getRingCount(matrix));
    }

    @Test
    public void test_replace() {
        List<List<Integer>> matrix = matrixDict.get("1");
        Integer element = matrix.get(0).get(0);
        assertEquals((Integer) 1, element);
    }
}